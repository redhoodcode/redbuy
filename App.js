import React from "react";
import { StyleSheet, Text, View } from "react-native";
import Cart from "./Components/Screens/Cart";
import ModelPicker from "./Components/ReusableComponents/ModelPicker";

export default function App() {
  return (
    <View style={styles.container}>
      <Cart />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "stretch",
  },
});
