import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  KeyboardAvoidingView,
  Image,
  ActivityIndicator,
  TouchableOpacity,
  FlatList,
  SafeAreaView,
  ScrollView,
} from "react-native";
import { Button } from "native-base";
import ModalPicker from "../ReusableComponents/ModelPicker";

import data from "../BackGround/myData.json";
// import { Button } from "native-base";

export default class Cart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cartData: data,
      isLoading: true,
      quantityValue: 3,
      size: "Small",
      getModalVisible: false,
      title: "",
    };
  }

  componentDidMount() {
    // fetch("https://jsonplaceholder.typicode.com/users")
    //   .then((response) => response.json())
    //   .then((data) => {
    //     this.setState({
    //       isLoading: false,
    //       cartData: data,
    //     });
    //   })
    //   .catch((error) => {
    //     console.log(error);
    //   });
    console.log(this.state.cartData);
    this.setState({
      isLoading: false,
    });
  }

  getSize = (item) => {
    // console.log("From Get Size-->",this.state.cartData);
    console.log("Item It is Bitch", item);

    this.setState({
      getModalVisible: true,
      title: "Size",
      currentItem: item,
    });
    console.log(this.state.currentItem);
  };
  getQuantity = () => {
    console.log("From Get Quantity");
    this.setState({
      getModalVisible: true,
      title: "Quantity",
    });
  };
  onClose = () => {
    console.log("From Close");

    this.setState({
      getModalVisible: false,
    });
  };
  render() {
    const data = this.state.cartData;
    let content;

    return (
      <SafeAreaView style={styles.containerCart}>
        <View style={styles.containerCart}>
          {this.state.isLoading ? (
            <View style={styles.container}>
              <ActivityIndicator size="large" />
            </View>
          ) : (
            <View style={styles.container}>
              <FlatList
                style={{ width: "100%" }}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                data={data}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => (
                  <View style={{ flex: 1 }}>
                    <View
                      style={{
                        flex: 1,
                        height: 200,
                        flexDirection: "row",
                        justifyContent: "space-evenly",
                        alignItems: "center",
                        alignSelf: "stretch",
                        alignContent: "center",

                        alignSelf: "stretch",
                      }}
                    >
                      <TouchableOpacity style={styles.imageContainer}>
                        <Image
                          source={{ uri: item.product.image }}
                          style={styles.imageView}
                        />
                      </TouchableOpacity>
                      <View style={styles.textContainer}>
                        <Text style={styles.productName}>
                          {item.product.name}
                        </Text>
                        <Text style={styles.productDiscription}>
                          {item.product.available}
                        </Text>
                        <View
                          style={{
                            flexDirection: "row",
                            justifyContent: "space-around",
                            height: 40,
                          }}
                        >
                          <TouchableOpacity
                            style={styles.button}
                            onPress={() => {
                              this.getSize(item.product);
                            }}
                          >
                            <Text>Size: {item.product.selectedSize}</Text>
                          </TouchableOpacity>
                          <TouchableOpacity
                            style={styles.button}
                            onPress={this.getQuantity.bind()}
                          >
                            <Text>Quantity: {item.product.quantity}</Text>
                          </TouchableOpacity>
                        </View>
                        <Text>{`\u20B9 ${item.product.price}`}</Text>
                      </View>
                    </View>
                    <View style={styles.buttonGroup}>
                      <Button full warning style={{ marginBottom: 2 }}>
                        <Text>Remove</Text>
                      </Button>
                      <Button full warning>
                        <Text>Move to Wishlist</Text>
                      </Button>
                    </View>
                  </View>
                )}

                // {this.state.cartData.map((element, key) => {
                //   return (
                //     <View key={key} style={styles.itemView}>
                //       <Image
                //         style={styles.imageView}
                //         source={{ uri: element.image }}
                //       />
                //       <Text>{element.product.id}</Text>
                //     </View>
                //   );
                // })}
              />
            </View>
          )}
        </View>
        {this.state.getModalVisible && (
          <ModalPicker
            currentItem={this.state.currentItem}
            title={this.state.title}
            controlToParent={this.onClose.bind()}
          />
        )}
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 30,
    width: "100%",
    height: "100%",
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "stretch",
  },
  containerCart: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    alignSelf: "stretch",
    margin: 10,
    marginBottom: 0,
  },
  itemView: {
    flex: 1,
    flexDirection: "row",
    alignSelf: "stretch",
  },
  imageButton: {
    width: "100%",
  },
  imageView: {
    width: "100%",
    height: 145,
    borderRadius: 7,
  },
  productName: {
    fontSize: 18,
    fontStyle: "normal",
  },
  productDiscription: {
    fontSize: 10,
    fontStyle: "italic",
  },
  imageContainer: {
    flex: 1,
    width: 130,
    height: 150,
    alignItems: "center",
  },
  textContainer: {
    flex: 1,
    width: 300,
    height: 150,
    marginLeft: 7,
  },
  button: {
    borderColor: "#000",
    borderWidth: 1,
    borderRadius: 10,
    margin: 10,
    padding: 5,
    justifyContent: "center",
  },
  buttonGroup: {
    padding: 0,
    margin: 0,
    display: "flex",
    alignItems: "stretch",
    alignContent: "space-between",
    width: "100%",
  },
});
