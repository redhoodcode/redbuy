import React, { Component } from 'react'
import {
    StyleSheet,
    Text,
    View, Picker} from "react-native";

export default class CustomPicker extends Component {
    constructor(props){
        super(props);
        this.state={
            selectedSize:'small',

        }
    }
    render() {
        return (
            <View style={styles.container}>
                <Picker
                    style={{width:"100%"}}
                    selectedValue={this.state.selectedSize}
                    onValueChange={(itemValue,itemIndex) => this.setState({
                        selectedSize:itemValue
                    })}
                >
                <Picker.Item label="Large" value="large" />
                <Picker.Item label="Medium" value="medium" />
                <Picker.Item label="Small" value="small" />
                </Picker>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      marginTop: 35,
      backgroundColor: "#fff",
      alignItems: "center",
      justifyContent: "center",
      alignSelf: "stretch",
    },
})