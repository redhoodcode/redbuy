import React, { Component } from "react";
import { Modal, Text, View, StyleSheet, Picker } from "react-native";
import { Icon } from "native-base";
import SwitchSelector from "react-native-switch-selector";

export default class ModelPicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentItem: this.props.currentItem,
      title: this.props.title,
    };
  }
  render() {
    console.log("Hi from ModelPicker");
    console.log("ModelData recieved-->>", this.state.currentItem.availableSize);
    return (
      <View>
        <Modal animationType="slide" visible transparent>
          <View style={styles.modelContainer}>
            <View style={styles.viewPicker}>
              <View style={styles.pickerHeader}>
                <Icon name="close" onPress={this.props.controlToParent} />
                <Text style={{ fontSize: 20 }}>{this.state.title}</Text>
                <Icon name="md-cart" />
              </View>
              <View style={styles.selectorView}>
                <SwitchSelector
                  options={this.state.currentItem.availableSize}
                  initial={0}
                  buttonColor="#eaa622"
                  onPress={(value) =>
                    console.log(`Call onPress with value: ${value}`)
                  }
                />
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  modelContainer: {
    flex: 1,
    backgroundColor: "rgba(0,0,0,0.5)",
    alignItems: "center",
    justifyContent: "flex-end",
  },
  viewPicker: {
    height: 200,
    width: "100%",
    backgroundColor: "#fff",
  },
  pickerHeader: {
    padding: 5,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignContent: "center",
    backgroundColor: "#dbce87",
  },
  selectorView:{
    flex:1,
    alignItems:"center",
    justifyContent:"center"
  },
});
